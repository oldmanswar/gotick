# gotick

__DISCLAIMER__: Use this application on your own risk. I do not take responsibility for any damage to you, your property, or others and others' property that may result from the use of it. Please be aware of the licensing terms.  

# Description
A web crawler for unlisted data buckets that's good  
But somewhat dark and written in go  
And therefore named precisely so  
Like a tick that crawls, looms and sucks out your blood  

In brief, this is a type of pen-testing tool designed to find open data containers in the cloud. I know that there are already many out there but they are written in Java or other interpreted languages (yes, Java needs a runtime and it is therefore interpreted) which makes them slow and heavy.  
There are also some online tools covering this area but they tend to be a little shady and you usually have to pay money to access the full set features.  
This tool is written in Go which makes it lightweight, fast and free under the eyes of GPL.  
If it is liked, it will be first of a series.  
Enjoy!
## Requirements
### Go
It is important to have an up to date version of the _golang_ compiler. Do not relay on distro provided versions as they are most of the times grossly outdated.  
The latest version can be downloaded from here: [Download Go Compiler](https://go.dev/dl/)  
Follow the installation instructions for your OS found here: [Install Go](https://go.dev/doc/install)  
In Linux you would do something like this:  
```sh
wget https://go.dev/dl/go1.20.5.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.20.5.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
```
You should also update your shell configuration to make the PATH permanent.  
Test the installation by issuing:
```sh
go version
```
### SQLite
__Gotick__ Writes its output to an SQLite file so it can be browsed in a flexible manner.  
Make sure you have appropriate software to browse this type of files.  
In Linux and just for the command line you can install SQLite by issuing:
```sh
sudo apt install sqlite
```
At the TOP of the TODO feature list are incorporating connectors for more powerful databases like PostgreSQL and MariaDB.  


## Download and Build
### Clone
You can clone the repo by cding into a directory of your choice and issuing:  
```sh
git clone git@gitlab.com:oldmanswar/gotick.git
```
### Build
CD to the newly created _gotick_ directory and build the project. There is only one command to be built right now, but this project may become a set of tools in the future so I have organised it accordingly.  
Beware that older versions of the golang compiler may have trouble finding the dependencies.  
```sh
go build cmd/gotick/gotick.go
```
If the command is successful you should have a newly created __gotick__ executable in the current directory.  
You can choose to move it somewhere in your executables path like _/usr/bin_:  
```sh
sudo mv gotick /usr/bin
```
The rest of the  READ ME assumes that _gotick_ is in your default path.
## Usage: Must Knows
* The true output of the application is an SQLite file. Its name SHOULD be provided in the command line. If no name is provided, the default value is _gotick.db_.
* Real time output is provided through the logging system. 
    * Gotick logs go to standard output. 
    * There are logging levels from 0 to 5. 
    * 0 is the most verbose one. (not recommended)
    * 1 is the default.
* You may provide a file for affixes. By default the app takes  _affixes.txt_ in the current directory. The list of affixes provided in the file is also hardcoded in the application so you do not have to take the file around when you change directories. Suggestions to improve this list are welcome.
## Usage: Main Features
__Gotick__ has three main functionalities: search, catalog and download. They can be used independently or all together but the latter mode poses some risks. It is best to run the different features sequentially and separately.  
### Search (crawler)
The search feature is used to look for data containers in the main cloud providers. Currently included are ___Google Cloud Storage , AWS S3, Azure Blob Storage and Digital Ocean Spaces___. These are not often indexed so we use a technique similar to that used by password crackers like John the Ripper or Hydra.  
This feature requires a file with words, each word in a single line, and a file with affixes (default one provided). From the combination of these two lists, a set of plausible bucket urls is created and probed for existence and traversability.  
To use this feature issue a command like the following:
```sh
gotick -words a_file_with_words.txt -db an_SQLite_file.db
```
You MUST provide a file with words, with each word being in its own line.  
You SHOULD provide the name of the database file where you want your output. If it doesn't exist, it will be created. If it exists, new data will be appended. If it is not provided, _gotick.db_ will be used by default.  
Alternatively to reading them from a file, you can generate the words by probing a domain's subdomains and use these as the set of words.  
To do this, issue:
```sh
gotick -domain a_domain.com -db an_SQLite_file.db
```
You MUST provide a valid existing domain in this particular case.  
### Catalog (loomer)
Once we have found a set of traversable data containers we want to know what is inside of them.  
The only requirement to use this feature is to have a previously created database containing traversable directories.  
Traversable directories are marked as both _existing_ and _listable_ in the database. They originate from urls generated by the crawler that have returned a __200 http status__.  
To use this feature issue a command like the following:
```sh
gotick -dir -db an_SQLite_file.db
```
You MUST provide the name of an existing gotick generated SQLite database.  
__WARNING__: It is impossible to know _a priori_ how many entries the directories may have. Experience tells me that those numbers can go up to many millions (some people use buckets as a backend for version control). This means that in some cases SQLite might not be up to the job. Use on your own risk.
### Download (sucker)
Once we have catalogued traversable data containers we can proceed to TRY to download the objects they contain. It is important to take into account that this type of services have very granular permission settings. This means that while objects may be listable some or all of them might not be downloadable.  
Other factor to take into account is the amount of objects and their sizes. It is not unusual to find data containers of sizes way larger than a standard hard drive. For this reason _Gotick_ has built in ways of filtering that allow to indicate which objects should be downloaded and which ones shouldn't. Through the command line we can indicate set of strings to keep and a set of strings to skip. If the name of the object has matching substrings in any of these sets it will be downloaded or skipped respectively.
To use this feature issue a command like the following:
```sh
gotick -download partial_domain_name -keep "coma,separated,strings" -skip "coma,separated,strings" -db an_SQLite_file.db
```
You MUST provide the name of an existing gotick generated SQLite database.  
You MUST provide a partial domain name to match the domains you want to consider for downloading.  
You SHOULD provide a list of strings to indicate which url paths you want to __keep__. For example "docx,pdf,csv".  
You SHOULD provide a list of strings to indicate which url paths you want to __skip__. For example "png,jpg,tiff".  
The downloaded files will be placed in a directory with the same name as the database and with the extension _.dl_.  
__WARNING__: The total size of the objects to be downloaded can be __HUGE__. A laptop or a desktop computer may not have an appropriately sized hard drive for the amount of information available. I recommend exploring and playing with the database file before attempting any downloads. Use on your own risk.
## Usage: Ancillary Features
### Concurrency
One of the main motivations to use Go is the handling of concurrency. Gotick can do many things in parallel but it is important keep in mind that CAN and SHOULD are two very different things.  
Gotick uses by default 15 concurrent url fetchers but it allows you to modify this number depending through the command line flag `-fetchers`. While we may think that the more the better, this is not true depending on the case.  
When we are crawling the web trying to find existing buckets, it is best to keep a low number of fetchers. Otherwise DNS providers (usually the same as the providers of the buckets) may classify your activity as suspicious and ban your requests temporarily (or, deity forbid, permanently).  
So maybe you will want to change the crawling command to something like this to not annoy the all powerful:  
```sh
gotick -words a_file_with_words.txt -db an_SQLite_file.db -fetchers 5
```
On the other hand, when retrieving directories and downloading files you will be dealing with the stuff the all powerful brag about in their one page holy texts (ads).  
So you may redo those commands as:

```sh
gotick -dir -db an_SQLite_file.db -fetchers 50
```
And:
```sh
gotick -download partial_domain_name -keep "coma,separated,strings" -skip "coma,separated,strings" -db an_SQLite_file.db -fetchers 100
```
Again: __Use on your own risk__.  
### Stealth
Sometimes we like to remain stealthy or test our runs from different IP addresses. Gotick allows you to do this using the `-tor` flag. You need the TOR browser installed and up un running to use this feature. Always remember that many of the TOR network exit nodes are run by law enforcement agencies. My recommendation is, do not use this application to do illegal or shady stuff.
Again: __Use on your own risk__.  
### Debugging
This is a beta release of the application. You will find bugs, and I will like to know about them. If you happen to run into a bug that it is also reproducible and you want to help fix it, run the app using the flag `-debug 0` and provide me with the output. Thanks.
## Viewing and Interpreting the Output
The output of the application is an SQLite database file. Beware that is in format version 3. If using the command line make sure you force version 3 by issuing:
```
sqlite3 an_SQLite_file.db
```
Omitting the 3 may lead to errors like "wrong format" or "database encrypted".  
Once opened, we can use any SQL statement of our liking to analyse the results. I am no SQL expert (as a matter of fact, I am not an expert in anything) but here are some examples you may find useful to view the data.
### List all surveyed domains
```sql
select * from domains;
```
### List existing surveyed domains
```sql
select * from domains where existing = 1;
```
### Show surveyed domains that can be listed
```sql
select * from domains where listable = 1;
```
### Count surveyed domains as a function of their http status
```sql
select count(*) as cnt,status,message,listable,existing from domains group by status;
```
### Count the entries in all listable domains
```sql
select count(*) as flcnt,domains.url as dom_url from files 
    left join domains on domains.hash=files.domain_hash
	group by files.domain_hash;
```
### Show files that might be pdfs
```sql
select * from files where url like "%pdf"
```
## Other Considerations
There would be many, but one I think it is important is that some administrators like to add some numbering to the names of their data containers so you may want to generate your list of words using one of the aforementioned password crackers. Just a suggestion. Again, don't use this for anything illegal. Inform the people you are surveying and alert them if you happen to find something that shouldn't be public.