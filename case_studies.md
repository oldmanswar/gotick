# Case Studies: Stripe, SCOTUS, Government of Perú, XNet
## Introduction
In the modern enterprise, data has become the lifeblood of business operations. Effective data management is crucial for making informed decisions, driving innovation, and gaining a competitive edge. Data containers play a vital role in organizing and storing information efficiently. However, in the context of the enterprise, misnaming data containers can lead to severe consequences that jeopardize data integrity, security, and the overall success of the organization.  

Enterprises operate in a highly regulated environment, with data protection and privacy regulations becoming increasingly stringent. Misnaming data containers can lead to data mishandling, unauthorized access, or improper disclosure of sensitive information. Non-compliance with data protection laws can result in substantial fines, legal liabilities, and reputation damage.  


## Stripe: Security by the Book, but...
Stripe is a financial software company with state of the art security. Yet, a generic scan combining its name with common data container affixes yields a series of results that give clues about where their data is stored and how their DevOps pipelines are organised.  
![Stripe Buckets](img/stripe.png "Stripe Buckets")  
Not all of these buckets are owned by Stripe, and the ones that are have very tight security regarding access, authorisation and authentication. Yet, affixes like development, staging, qa and production say a lot about the flow of information within the company.
At the same time, regardless of their level of security, this type of naming gives hackers very clear hints about where to start looking for data.

## SCOTUS: Should this Be Public?
The Supreme Court of the United States provides several sites that allow for the public to search for information on ongoing and past legal cases. One example is the Circuit Nine site accessible from this link: <https://www.ca9.uscourts.gov/>  
The search engine powering the site is provided by Google and works very well. There is, however, an issue that might be a bug: some of the results returned are clearly marked as __NOT FOR PUBLICATION__  
![Not for publication](img/nfp.png "Not for publication")  
Should documents marked this way be available to the public?  
It is questionable as it gives a plethora of personal and private information.  
Further more, all the information provided in the site is also available in a public and anonymously accessible Amazon S3 container located at:  
![Circuit 9](img/circ9.png "Circuit 9")  
This allows third parties to access the bulk of the data to use it for their own purposes like private search engines or large language model training.  


## Government of Perú: No comments.
This particular case would be the perfect example of blatant carelessness. At the time of this writing, it was not possible to find an official site that offers the information openly available in this bucket which seems to belong to the Government of Perú.  
Not only we find here millions of documents containing personal and private data, but also clues on the type of operating system and the applications used by the country's administration.  
This bucket is an open invitation for hackers and data parasites. And, again, it starts by using the wrong name for a data container.  
![GobPe](img/gobpe.png "GobPe")  

## XNet: the opposite case.
XNet is a counter case to all the previous ones. Here we find a fairly large set of buckets that look to be the property of the activist organisation <https://xnet-x.net/es/>.  
![XNet](img/xnet.png "XNet")  
After speaking with organisation officials it was found that none are their property. We can speculate that some might be a matter of naming coincidence and others an effort of gathering and storing information on them by some third party.  
Either way, the choice of names remains unfortunate from a security point of view.

## Conclusion
The first measure to keep data secure should be to keep it hidden. Data containers from the main cloud providers use the public DNS system and are therefore easy to locate. The best course of action is to name our buckets as randomly as possible to prevent dictionary attacks on them. Bucket naming should be treated the same way as password picking.