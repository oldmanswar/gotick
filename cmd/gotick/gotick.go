package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog"

	"gitlab.com/oldmanswar/gotick/pkg/bucket"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

var help bool
var tor bool
var dirs bool
var wordsFile string
var affixesFile string
var dbName string
var logLevel int
var simulFetch int
var download string
var keep string
var skip string

var domain string

// var repo string

var words []string
var skipList []string
var keepList []string

var logger zerolog.Logger

func readWords(fileName string) []string {
	buffer, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Println("Could not open file.")
		return nil
	}
	text := string(buffer)

	return strings.Split(text, "\n")
}

func parseFlags(ctx context.Context) bool {
	proceed := false

	flag.BoolVar(&help, "help", false, "Show help")
	flag.BoolVar(&dirs, "dir", false, "Retrieve available directories.")
	flag.StringVar(&domain, "domain", "", "Retrieve subdomains of given domain.")
	//flag.StringVar(&repo, "bucket", "", "A url for a bucket to retrieve its directory.")
	flag.BoolVar(&tor, "tor", false, "Use TOR proxy")
	flag.StringVar(&wordsFile, "words", "", "A file containing words to create bucket url candidates.")
	flag.StringVar(&affixesFile, "affixes", "affixes.txt", "A file containing affixes to combine with the words.")
	flag.StringVar(&dbName, "db", "gotick.db", "A SQLite file to output the results.")
	flag.StringVar(&download, "download", "", "A string matching the domains to download.")
	flag.StringVar(&keep, "keep", "", "A list of comma separated strings matching the names of the objects to download.")
	flag.StringVar(&skip, "skip", "", "A list of comma separated strings matching the names of the objects to be ignored.")
	flag.IntVar(&logLevel, "log", 1, "Log level (0 -> 5)")
	flag.IntVar(&simulFetch, "fetchers", 15, "Maximum concurrent http clients.")
	flag.Parse()

	if help {
		return proceed
	}
	/*
		if repo != "" {
			proceed = true
		}
	*/
	if wordsFile != "" {
		words = readWords(wordsFile)
		if words == nil {
			fmt.Println("Could not open words file. Exiting.")
			os.Exit(1)
		}
		proceed = true
	}

	if affixesFile != "" {
		newAffixes := readWords(affixesFile)
		if newAffixes != nil {
			tools.SetAffixes(newAffixes)
		}
	}

	if domain != "" {
		words = strings.Split(domain, ".")[:1]
		subdomain := tools.CreateStdSubfinder()
		newAffixes := append(tools.Affixes(), subdomain.Run(ctx, domain)...)
		if newAffixes != nil {
			tools.SetAffixes(tools.NormaliseSubs(newAffixes))
		}
		proceed = true
	}

	if dirs {
		proceed = true
	}

	if download != "" {
		proceed = true
		if skip != "" {
			skipList = strings.Split(skip, ",")
		}
		if keep != "" {
			keepList = strings.Split(keep, ",")
		}
	}

	if tor {
		tools.UseProxy("socks5://localhost:9150/")
	}

	zerolog.SetGlobalLevel(zerolog.Level(logLevel))

	return proceed
}

func retrieveAllDirectories(ctx context.Context, keeper *bucket.Keeper) {
	var wg sync.WaitGroup
	logger.Info().Msg("retrieveAllDirectories")
	//keeper.WaitForExplorers()
	available := keeper.ReadAvailableDomains()
	logger.Info().Msgf("available domains %d", len(available))
	for i, directory := range available {
		logger.Info().Msgf("retrieving dir %s", directory.Url())
		wg.Add(1)
		keeper.InitChecks()
		go func(directory *bucket.DomainRecord, i int) {
			bucket.RetrieveDirectory(ctx, keeper, directory)
			logger.Info().Msgf("retrieved %d", i+1)
			wg.Done()
			keeper.EndChecks()
		}(directory, i)
	}
	logger.Info().Msg("directory loop end: waiting")
	wg.Wait()
	logger.Info().Msg("directory loop end: done")
}

func main() {
	logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		With().Timestamp().
		Str("application", "gotick.go").
		Logger()

	ctx := context.WithValue(context.Background(), "logger", &logger)

	if !parseFlags(ctx) {
		flag.Usage()
		os.Exit(0)
	}

	keeper, err := bucket.NewKeeper(ctx, dbName)
	if err != nil {
		logger.Panic().Msg("could not initialise the database " + err.Error())
	}

	domFetcher := bucket.NewDomainFetcher(ctx, keeper, simulFetch)
	fileFetcher := bucket.NewFileFetcher(ctx, keeper, simulFetch)

	go func() {
		keeper.Listen(ctx)
		logger.Warn().Msg("can't hear anything anymore")
	}()

	if dirs {
		keeper.InitChecks() // TODO need more elegance here
	}
	/*
		if repo != "" {
			//bucket.RetrieveDirectory(ctx, keeper, repo)
		}
	*/
	if download != "" {
		keeper.InitChecks() // TODO need more elegance here
	}
	if words != nil {
		logger.Warn().Msg("starting up")
		bucket.Explore(ctx, keeper, words, tools.Affixes())
	} else {
		logger.Warn().Msg("no exploring action performed")
	}

	if dirs {
		retrieveAllDirectories(ctx, keeper)
		keeper.EndChecks() // TODO need more elegance here
	}

	if download != "" {
		keeper.DownloadFiles(ctx, download, keepList, skipList)
		keeper.EndChecks() // TODO need more elegance here
	}

	logger.Info().Msg("waiting for what?")
	//wg.Wait()
	domFetcher.Stop()
	fileFetcher.Stop()
	keeper.Close()
	logger.Warn().Msg("scan finished")
}
