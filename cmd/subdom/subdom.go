package main

import (
	"context"
	"os"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

func main() {

	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		With().Timestamp().
		Str("application", "subdom.go").
		Logger()

	ctx := context.WithValue(context.Background(), "logger", &logger)
	subdomain := tools.CreateStdSubfinder()
	sources := subdomain.SourceList()
	for _, source := range sources {
		logger.Debug().Msg(source)
	}

	if len(os.Args) != 2 {
		logger.Panic().Msg("this program takes exactly one argument")
	}

	results := subdomain.Run(ctx, os.Args[1])
	for i, result := range results {
		logger.Warn().Msgf("%d %s", i, result)
	}
}
