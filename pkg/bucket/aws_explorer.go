package bucket

import (
	"context"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

type awsExplorer struct {
	logger *zerolog.Logger
	keeper *Keeper
}

func newAwsExplorer(ctx context.Context, keeper *Keeper) Explorer {
	explorer := new(awsExplorer)
	explorer.logger = tools.LoggerForContext(ctx, "awsExplorer")
	explorer.keeper = keeper
	return explorer
}

func (bkt *awsExplorer) Word2Url(word string) string {
	return "https://" + word + ".s3.amazonaws.com/"
}

func (bkt *awsExplorer) WordAffix2Urls(word string, affix string) []string {
	urls := make([]string, 0, 4)

	urls = append(urls, "https://"+word+affix+".s3.amazonaws.com/")
	urls = append(urls, "https://"+affix+word+".s3.amazonaws.com/")
	urls = append(urls, "https://"+word+"-"+affix+".s3.amazonaws.com/")
	urls = append(urls, "https://"+affix+"-"+word+".s3.amazonaws.com/")

	return urls
}

func (xpl *awsExplorer) Explore(ctx context.Context, words []string, affixes []string) {
	explore(ctx, xpl, words, affixes)
}

func (xpl *awsExplorer) Logger() *zerolog.Logger {
	return xpl.logger
}

func (xpl *awsExplorer) Keeper() *Keeper {
	return xpl.keeper
}
