package bucket

import (
	"context"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

// DomainFetcher in an implementation of the Fetcher interface for data bucket directories
type DomainFetcher struct {
	logger *zerolog.Logger
	keeper *Keeper
	fechan chan string
	cancel context.CancelFunc
	wg     sync.WaitGroup
}

// NewDomainFetcher instantiates an asynchronous fetcher for bucket ulrls
// The downloaded page is redirected to the Keeper for processing
// Concurrency can be regulated through the maxSymul parameter
func NewDomainFetcher(ctx context.Context, keeper *Keeper, maxSimul int) *DomainFetcher {
	fetcher := new(DomainFetcher)
	fetcher.logger = tools.LoggerForContext(ctx, "DomainFetcher")
	fetcher.keeper = keeper
	keeper.SetDomainFetcher(fetcher)
	fetcher.fechan = make(chan string)
	var fCtx context.Context
	fCtx, fetcher.cancel = context.WithCancel(ctx)
	for i := 0; i < maxSimul; i++ {
		go func() {
			fetcher.listener(fCtx)
		}()
	}

	return fetcher
}

// fetch is the workhorse function
func (fetcher *DomainFetcher) fetch(ctx context.Context, url string) {
	logger := tools.LoggerForFunction(fetcher.logger, "fetch")
	logger.Debug().Msg("fetching url " + url)

	record := NewDomain(url)

	retries := 0
	retry := true
	for retry {
		_, status, errString := tools.GetUrl(ctx, url)
		record.httpStatus = int16(status)
		record.message = errString
		logger.Debug().Msgf("http status %d", status)
		switch status {
		case 200:
			logger.Info().Int("status", status).Msg("listable match found for " + url)
			record.listable = true
			record.existing = true
			record.message = "success"
			retry = false
		case 403:
			logger.Info().Int("status", status).Msg("protected match found for " + url)
			record.existing = true
			record.message = "forbidden"
			retry = false
		case 404:
			logger.Debug().Int("status", status).Msg("not found")
			record.existing = false
			record.message = "bucket not found"
			retry = false
		default:
			if strings.Contains(errString, "host unreachable") {
				logger.Warn().Msgf("host unreachable: internet down? (%d)", retries)
				logger.Warn().Msg(errString)
				time.Sleep(10 * time.Second)
				if retries < 6 {
					retries++
					continue
				} else {
					record.message = "host unreachable"
					retry = false
				}
			} else if strings.Contains(errString, "access denied") {
				logger.Warn().Msgf("access denied: pissing them off? (%d)", retries)
				time.Sleep(10 * time.Second)
				if retries < 6 {
					retries++
					continue
				} else {
					record.message = "access denied"
					retry = false
					break
				}
			} else if strings.Contains(errString, "no such host") {
				retry = false
				break
			} else {
				logger.Warn().Msgf("weird stuff %d %s", status, errString)
				retry = false
			}
		}
	}
	fetcher.keeper.WriteDomain(record)
}

// Stop deactivates the fetcher
func (fetcher *DomainFetcher) Stop() {
	fetcher.logger.Info().Msg("Stop")
	fetcher.wg.Wait()
	fetcher.cancel()
	fetcher.keeper.RemoveDomainFetcher()
	close(fetcher.fechan)
}

// fetch  listens for download requests
func (fetcher *DomainFetcher) listener(ctx context.Context) {
	keepGoing := true
	for keepGoing {
		select {
		case url := <-fetcher.fechan:
			fetcher.fetch(ctx, url)
			fetcher.wg.Done()
		case <-ctx.Done():
			keepGoing = false
		}
	}
}

// Fetch requests the downloading of a bucket url asynchronously
func (fetcher *DomainFetcher) Fetch(ctx context.Context, obj ...any) {
	fetcher.wg.Add(1)
	url := obj[0].(string)
	fetcher.logger.Debug().Msg("fetch request url " + url)
	fetcher.fechan <- url
}
