package bucket

import (
	"context"
	"sync"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

type Explorer interface {
	Word2Url(word string) string
	WordAffix2Urls(word string, affix string) []string
	Explore(ctx context.Context, words []string, affixes []string)
	Logger() *zerolog.Logger
	Keeper() *Keeper
}

func explore(ctx context.Context, xpl Explorer, words []string, affixes []string) {
	xpl.Keeper().InitChecks()
	for _, word := range words {
		xpl.Logger().Debug().Msg("droning for " + word)
		url := xpl.Word2Url(word)
		xpl.Logger().Debug().Msg("url " + url)
		xpl.Keeper().CheckDomain(ctx, url)
		xpl.Logger().Debug().Msg("chk " + url)
		for _, affix := range affixes {
			if word == "" || affix == "" {
				continue
			}
			urls := xpl.WordAffix2Urls(word, affix)
			for _, url := range urls {
				xpl.Logger().Debug().Msg("url " + url)
				xpl.Keeper().CheckDomain(ctx, url)
				xpl.Logger().Debug().Msg("chk " + url)
			}
		}
	}
	xpl.Keeper().EndChecks()
	xpl.Logger().Warn().Msg("done exploring")
}

func ProbeDomain(ctx context.Context, domain string) {
	logger := tools.LoggerForContext(ctx, "ProbeDomain")
	logger.Debug().Msg(domain)
}

func Explore(ctx context.Context, keeper *Keeper, words []string, affixes []string) {
	logger := tools.LoggerForContext(ctx, "Explore")
	var wg sync.WaitGroup

	aws := newAwsExplorer(ctx, keeper)
	azure := newAzureExplorer(ctx, keeper)
	google := newGoogleExplorer(ctx, keeper)
	ocean := newOceanExplorer(ctx, keeper)

	// copy word arrays to avoid concurrency issues
	words1 := tools.DeepDuplicate(words)
	words2 := tools.DeepDuplicate(words)
	words3 := tools.DeepDuplicate(words)
	words4 := tools.DeepDuplicate(words)

	affixes1 := tools.DeepDuplicate(affixes)
	affixes2 := tools.DeepDuplicate(affixes)
	affixes3 := tools.DeepDuplicate(affixes)
	affixes4 := tools.DeepDuplicate(affixes)

	for _, word := range words4 {
		logger.Debug().Msg(word)
	}

	wg.Add(1)
	go func() {
		aws.Explore(ctx, words1, affixes1)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		azure.Explore(ctx, words2, affixes2)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		google.Explore(ctx, words3, affixes3)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		ocean.Explore(ctx, words4, affixes4)
		wg.Done()
	}()
	logger.Warn().Msg("waiting for children")
	wg.Wait()
	logger.Warn().Msg("children are home")
}
