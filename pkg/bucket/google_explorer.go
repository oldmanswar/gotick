package bucket

import (
	"context"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

type googleExplorer struct {
	logger *zerolog.Logger
	keeper *Keeper
}

func newGoogleExplorer(ctx context.Context, keeper *Keeper) Explorer {
	explorer := new(googleExplorer)
	explorer.logger = tools.LoggerForContext(ctx, "googleExplorer")
	explorer.keeper = keeper

	return explorer
}

func (bkt *googleExplorer) Word2Url(word string) string {
	return "https://" + word + ".storage.googleapis.com/"
}

func (bkt *googleExplorer) WordAffix2Urls(word string, affix string) []string {
	urls := make([]string, 0, 4)

	urls = append(urls, "https://"+word+affix+".storage.googleapis.com/")
	urls = append(urls, "https://"+affix+word+".storage.googleapis.com/")
	urls = append(urls, "https://"+word+"-"+affix+".storage.googleapis.com/")
	urls = append(urls, "https://"+affix+"-"+word+".storage.googleapis.com/")

	return urls
}

func (xpl *googleExplorer) Explore(ctx context.Context, words []string, affixes []string) {
	explore(ctx, xpl, words, affixes)
}

func (xpl *googleExplorer) Logger() *zerolog.Logger {
	return xpl.logger
}

func (xpl *googleExplorer) Keeper() *Keeper {
	return xpl.keeper
}
