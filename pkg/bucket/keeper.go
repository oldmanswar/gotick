package bucket

import (
	"context"
	"database/sql"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

const createDomainsTable string = `
  CREATE TABLE IF NOT EXISTS "domains" (
	"hash"	INTEGER NOT NULL,
	"url"	TEXT NOT NULL,
	"message" TEXT,
	"status"	INTEGER,
	"existing"	INTEGER,
	"listable"	INTEGER,
	"created"	INTEGER,
	"modified"	INTEGER,
	"inspected"	INTEGER,
	PRIMARY KEY("hash")
);`

const insertDomain string = `INSERT INTO "domains" (
	"hash",
	"url",
	"message",
	"status",
	"existing",
	"listable",
	"created",
	"modified",
	"inspected"
	) VALUES (?,?,?,?,?,?,?,?,?);`

const readDomainSQL string = `SELECT 
	"hash",
	"url",
	"message",
	"status",
	"existing",
	"listable",
	"created",
	"modified",
	"inspected" 
	FROM "domains" WHERE "hash" = ?`

const readAvailableDomainsSql string = `SELECT 
	"hash",
	"url",
	"message",
	"status",
	"existing",
	"listable",
	"created",
	"modified",
	"inspected" 
	FROM "domains" WHERE "listable" = 1`

const readAvailableMatchingDomainsSql string = `SELECT 
	"hash",
	"url",
	"message",
	"status",
	"existing",
	"listable",
	"created",
	"modified",
	"inspected" 
	FROM "domains" WHERE "listable" = 1 AND url like ?`

type DomainRecord struct {
	hash       int64
	existing   bool
	listable   bool
	httpStatus int16
	created    int64
	modified   int64
	inspected  int64
	url        string
	message    string
}

func NewDomain(url string) *DomainRecord {
	record := new(DomainRecord)
	record.hash = tools.FindHash(url)
	record.url = url
	record.httpStatus = 0
	record.existing = false
	record.listable = false
	record.created = time.Now().Unix()
	record.modified = time.Now().Unix()
	record.inspected = time.Now().Unix()
	return record
}

func (rec *DomainRecord) Url() string {
	return rec.url
}

const createFilesTableSQL string = `
	CREATE TABLE IF NOT EXISTS "files" (
	  "hash"	INTEGER NOT NULL,
	  "domain_hash"	INTEGER NOT NULL,
	  "url"	TEXT NOT NULL,
	  "message" TEXT,
	  "size" INTEGER,
	  "status"	INTEGER,
	  "created"	INTEGER,
	  "modified"	INTEGER,
	  "inspected"	INTEGER,
	  "downloaded"	INTEGER,
	  PRIMARY KEY("hash")
  );`

const insertFileSql string = `INSERT INTO "files" (
	"hash",
	"domain_hash",
	"url",
	"message",
	"size",
	"status",
	"created",
	"modified",
	"inspected",
	"downloaded"
	) VALUES (?,?,?,?,?,?,?,?,?,?);`

const updateFileSql string = `UPDATE "files" SET
		message = ?,
		status = ?,
		downloaded = ?
		WHERE hash=?;`

const readFileSql string = `SELECT 
	"hash",
	"domain_hash",
	"url",
	"message",
	"size",
	"status",
	"created",
	"modified",
	"inspected",
	"downloaded"
	FROM "files" WHERE "hash" = ?`

const readDomainFilesSql string = `SELECT 
	"hash",
	"domain_hash",
	"url",
	"message",
	"size",
	"status",
	"created",
	"modified",
	"inspected",
	"downloaded"
	FROM "files" WHERE "domain_hash" = ?`

const readDomainFileHashesSql string = `SELECT 
	"hash"
	FROM "files" WHERE "domain_hash" = ?`

type FileRecord struct {
	hash       int64
	domainHash int64
	httpStatus int16
	created    int64
	modified   int64
	inspected  int64
	downloaded int64
	size       int64
	url        string
	message    string
}

func NewFile(domainHash int64, url string) *FileRecord {
	record := new(FileRecord)
	record.hash = tools.FindHash(url)
	record.domainHash = domainHash
	record.url = url
	record.httpStatus = 0
	record.downloaded = time.Now().Unix()
	record.created = time.Now().Unix()
	record.modified = time.Now().Unix()
	record.inspected = time.Now().Unix()
	return record
}

type MessageAction int

const (
	Init MessageAction = iota
	Record
	Update
	Write
	End
)

type RecordType int

const (
	Domain RecordType = iota
	File
)

type UrlMessage interface {
	Url() string
	Type() RecordType
	Action() MessageAction
	Domain() *DomainRecord
	File() *FileRecord
	Body() []byte
}

type urlMessage struct {
	url     string
	action  MessageAction
	urlType RecordType
	domain  *DomainRecord
	file    *FileRecord
	body    []byte
}

type fileDomain struct {
	domain *DomainRecord
	file   *FileRecord
	body   []byte
}

func (msg *urlMessage) Url() string {
	return msg.url
}

func (msg *urlMessage) Action() MessageAction {
	return msg.action
}

func (msg *urlMessage) Type() RecordType {
	return msg.urlType
}

func (msg *urlMessage) Domain() *DomainRecord {
	return msg.domain
}

func (msg *urlMessage) Body() []byte {
	return msg.body
}

func (msg *urlMessage) File() *FileRecord {
	return msg.file
}

type Keeper struct {
	db            *sql.DB
	baseDir       string
	logger        *zerolog.Logger
	wg            sync.WaitGroup
	mtxDomain     sync.Mutex
	mtxFile       sync.Mutex
	mtxKeep       sync.Mutex
	domFetcher    Fetcher
	fileFetcher   Fetcher
	domainWriter  chan *DomainRecord
	recordChecker chan UrlMessage
	fileWriter    chan UrlMessage
	fileChecker   chan UrlMessage
	waiter        chan bool
	waiters       []chan bool
}

func NewKeeper(ctx context.Context, fileName string) (*Keeper, error) {
	var err error

	keeper := new(Keeper)
	keeper.domFetcher = nil
	keeper.fileFetcher = nil
	keeper.logger = tools.LoggerForContext(ctx, "Keeper")
	keeper.db, err = sql.Open("sqlite3", fileName)
	if err != nil {
		keeper.logger.Panic().Msg("cannot open file " + fileName)
		return nil, err
	}

	if _, err := keeper.db.Exec(createDomainsTable); err != nil {
		keeper.logger.Panic().Msg("cannot create table domains")
		return nil, err
	}

	if _, err := keeper.db.Exec(createFilesTableSQL); err != nil {
		keeper.logger.Panic().Msg("cannot create table files")
		return nil, err
	}
	keeper.baseDir = strings.Split(fileName, ".")[0] + ".dl/"
	keeper.domainWriter = make(chan *DomainRecord)
	keeper.recordChecker = make(chan UrlMessage)
	keeper.fileWriter = make(chan UrlMessage)
	keeper.fileChecker = make(chan UrlMessage)
	keeper.waiter = make(chan bool)

	return keeper, nil
}

func (keeper *Keeper) Debug(message string) {
	keeper.logger.Debug().Msg(message)
}

func (keeper *Keeper) SetDomainFetcher(fetcher Fetcher) {
	keeper.domFetcher = fetcher
}

func (keeper *Keeper) RemoveDomainFetcher() {
	keeper.domFetcher = nil
}

func (keeper *Keeper) SetFileFetcher(fetcher Fetcher) {
	keeper.fileFetcher = fetcher
}

func (keeper *Keeper) RemoveFileFetcher() {
	keeper.fileFetcher = nil
}

func (keeper *Keeper) addWaiter(waiterChan chan bool) {
	keeper.waiters = append(keeper.waiters, waiterChan)
}

func (keeper *Keeper) WaitForExplorers() {
	time.Sleep(5 * time.Second)
	waiter := make(chan bool)
	keeper.addWaiter(waiter)
	keeper.logger.Info().Msg("send dummy message")
	keeper.waiter <- true
	if <-waiter {
		keeper.logger.Info().Msg("domain retrieving done")
		close(waiter)
	}
}

func (keeper *Keeper) writeDomain(domain *DomainRecord) bool {
	keeper.Debug("writeDomain " + domain.url)
	if keeper.readDomain(domain.url) != nil {
		return true
	}
	_, err := keeper.db.Exec(insertDomain,
		domain.hash,
		domain.url,
		domain.message,
		domain.httpStatus,
		domain.existing,
		domain.listable,
		domain.created,
		domain.modified,
		domain.inspected)
	if err != nil {
		keeper.logger.Error().Msg("database domain write error " + err.Error())
	}
	return err == nil
}

func (keeper *Keeper) WriteDomain(domain *DomainRecord) {
	keeper.Debug("WriteDomain " + domain.url)
	keeper.domainWriter <- domain
}

func (keeper *Keeper) readDomain(url string) *DomainRecord {
	keeper.Debug("readDomain " + url)
	hash := tools.FindHash(url)
	row := keeper.db.QueryRow(readDomainSQL, hash)

	record := DomainRecord{}

	err := row.Scan(&record.hash, &record.url, &record.message, &record.httpStatus, &record.existing,
		&record.listable, &record.created, &record.modified, &record.inspected)

	if err == nil {
		keeper.logger.Debug().Msgf("%d %s %s", hash, url, record.url)
		if url != record.url {
			keeper.logger.Panic().Msg("hash colision, bailing out")
		}
		return &record
	}

	if err == sql.ErrNoRows {
		keeper.logger.Debug().Msg("url not found")
	} else {
		keeper.logger.Panic().Msg("Scan error " + err.Error())
		return nil
	}
	return nil
}

func (keeper *Keeper) ReadDomain(url string) *DomainRecord {
	keeper.Debug("ReadDomain " + url)
	keeper.mtxDomain.Lock()
	record := keeper.readDomain(url)
	keeper.mtxDomain.Unlock()
	keeper.Debug("END ReadDomain " + url)
	return record
}

func (keeper *Keeper) readDomains(sql string, args ...any) []*DomainRecord {
	keeper.mtxDomain.Lock()
	defer keeper.mtxDomain.Unlock()
	if args != nil && args[0] != nil {
		args[0] = "%" + args[0].(string) + "%"
	}
	rows, err := keeper.db.Query(sql, args...)
	if err != nil {
		keeper.logger.Error().Msgf("read domains (%d) query error %s", len(args), err.Error())
		return nil
	}
	defer rows.Close()
	records := make([]*DomainRecord, 0) // just because, TODO find length first
	for rows.Next() {
		record := new(DomainRecord)
		err := rows.Scan(&record.hash, &record.url, &record.message, &record.httpStatus, &record.existing,
			&record.listable, &record.created, &record.modified, &record.inspected)
		keeper.logger.Debug().Msg(record.url)
		if err != nil {
			keeper.logger.Error().Msg("scan error " + err.Error())
			return nil
		}
		records = append(records, record)
	}
	return records
}

func (keeper *Keeper) ReadAvailableDomains() []*DomainRecord {
	return keeper.readDomains(readAvailableDomainsSql)
}

func (keeper *Keeper) ReadAvailableMatchingDomains(hint string) []*DomainRecord {
	return keeper.readDomains(readAvailableMatchingDomainsSql, hint)
}

func (keeper *Keeper) updateFile(file *FileRecord) bool {
	_, err := keeper.db.Exec(updateFileSql,
		file.message,
		file.httpStatus,
		file.downloaded,
		file.hash,
	)
	if err != nil {
		keeper.logger.Error().Msg("updateFile: database write file error " + err.Error())
	}
	return err == nil
}

func (keeper *Keeper) writeFile(file *FileRecord, action MessageAction) bool {
	if action == Update {
		return keeper.updateFile(file)
	}
	if keeper.readFile(file.hash) != nil {
		return true
	}
	_, err := keeper.db.Exec(insertFileSql,
		file.hash,
		file.domainHash,
		file.url,
		file.message,
		file.size,
		file.httpStatus,
		file.created,
		file.modified,
		file.inspected,
		file.downloaded,
	)
	if err != nil {
		keeper.logger.Error().Msg("writeFile: database write file error " + err.Error())
	}
	return err == nil
}

func (keeper *Keeper) WriteFile(file *fileDomain, action MessageAction, body []byte) {
	msg := urlMessage{action: action, body: body, domain: file.domain, file: file.file, urlType: RecordType(File)}
	keeper.logger.Debug().Msgf("WriteFile %d body %t", action, body != nil)
	keeper.fileWriter <- &msg
}

func (keeper *Keeper) readFile(hash int64) *FileRecord {
	row := keeper.db.QueryRow(readFileSql, hash)

	record := FileRecord{}

	err := row.Scan(&record.hash, &record.domainHash, &record.url, &record.message, &record.size, &record.httpStatus,
		&record.created, &record.modified, &record.inspected, &record.downloaded)

	if err == nil {
		return &record
	}

	if err == sql.ErrNoRows {
		keeper.logger.Debug().Msg("hash not found")
	} else {
		keeper.logger.Panic().Msg("Scan error " + err.Error())
		return nil
	}
	return nil
}

func (keeper *Keeper) ReadFile(url string) *FileRecord {
	hash := tools.FindHash(url)
	keeper.mtxFile.Lock()
	record := keeper.readFile(hash)
	keeper.mtxFile.Unlock()
	return record
}

func (keeper *Keeper) InitChecks() {
	keeper.Debug("InitChecks")
	msg := urlMessage{action: Init}
	keeper.recordChecker <- &msg
}

func (keeper *Keeper) CheckDomain(ctx context.Context, url string) {
	keeper.Debug("CheckDomain")
	if keeper.ReadDomain(url) == nil {
		keeper.domFetcher.Fetch(ctx, url)
		keeper.logger.Debug().Msg("FETCH DONE")
	}
}

func (keeper *Keeper) CheckFile(domain *DomainRecord, file *FileRecord) {
	keeper.Debug("CheckFile")
	msg := urlMessage{action: Record, domain: domain, file: file, urlType: RecordType(File)}
	keeper.recordChecker <- &msg
}

func (keeper *Keeper) EndChecks() {
	keeper.Debug("EndChecks")
	msg := urlMessage{action: End}
	keeper.recordChecker <- &msg
}

func (keeper *Keeper) DomainWriterChannel() chan *DomainRecord {
	return keeper.domainWriter
}

func (keeper *Keeper) DomainCheckerChannel() chan UrlMessage {
	return keeper.recordChecker
}

/*
	func (keeper *Keeper) fetchDomain(ctx context.Context, url string) int {
		keeper.Debug("fetchDomain")
		if keeper.ReadDomain(url) == nil {
			keeper.domFetcher.Fetch(ctx, url)
			keeper.logger.Debug().Msg("FETCH DONE")
			return 1
		}
		return 0
	}
*/
func (keeper *Keeper) readDomainFileHashes(hash int64) []int64 {
	keeper.mtxFile.Lock()
	defer keeper.mtxFile.Unlock()
	rows, err := keeper.db.Query(readDomainFileHashesSql, hash)
	if err != nil {
		keeper.logger.Error().Msg("readDomainFileHashes query error " + err.Error())
		return nil
	}
	var hashes []int64
	for rows.Next() {
		var value int64
		rows.Scan(&value)
		hashes = append(hashes, value)
	}
	return hashes
}

func (keeper *Keeper) saveFile(ctx context.Context, domain *DomainRecord, file *FileRecord, body []byte) bool {
	keeper.Debug("saveFile")
	domParts := strings.Split(domain.url, "/")
	fileDir := keeper.baseDir + domParts[2] + "/" +
		file.url[:strings.LastIndex(file.url, "/")+1]
	fileName := file.url[strings.LastIndex(file.url, "/")+1:]

	if os.MkdirAll(fileDir, os.ModePerm) != nil {
		keeper.logger.Error().Msg("failed to create directory " + fileDir)
		return false
	}
	err := os.WriteFile(fileDir+fileName, body, 0644)
	if err != nil {
		keeper.logger.Error().Msgf("failed to write file %s error %s", fileName, err.Error())
		return false
	}
	return true
}

func (keeper *Keeper) IsForKeeps(str string, keep, skip []string) bool {
	lowStr := strings.ToLower(str)

	keeper.mtxKeep.Lock()
	defer keeper.mtxKeep.Unlock()
	for _, word := range skip {
		keeper.logger.Debug().Msg("skip word " + word)
		if strings.Contains(lowStr, word) {
			keeper.logger.Debug().Msg("Keep 1 false")
			return false
		}
	}
	if len(keep) == 0 {
		keeper.logger.Debug().Msg("Keep 2 true")
		return true
	}
	for _, word := range keep {
		if strings.Contains(lowStr, word) {
			keeper.logger.Debug().Msg("Keep 3 true")
			return true
		}
	}
	keeper.logger.Debug().Msg("Keep 4 false")
	return false

}

func (keeper *Keeper) downloadDomainFiles(ctx context.Context, domain *DomainRecord, keep, skip []string) {
	fileHashes := keeper.readDomainFileHashes(domain.hash)
	if fileHashes == nil {
		keeper.logger.Error().Msg("error retrieving file hashes for domain " + domain.url)
	}
	keeper.logger.Debug().Msgf("domain files %d", len(fileHashes))
	for i, hash := range fileHashes {
		keeper.mtxFile.Lock()
		record := keeper.readFile(hash)
		keeper.mtxFile.Unlock()
		if record != nil && record.downloaded == 0 { // it is best to not duplicate downloads but an override might be needed
			keeper.logger.Debug().Msgf("%d considering file %s", i, record.url)
			fullUrl := domain.url + url.QueryEscape(record.url)
			if keeper.IsForKeeps(fullUrl, keep, skip) {
				keeper.logger.Debug().Msgf("%d downloading file %s", i, fullUrl)
				keeper.fileFetcher.Fetch(ctx, domain, record)
			}
		}
	}
}

func (keeper *Keeper) DownloadFiles(ctx context.Context, hint string, keep, skip []string) {
	keeper.logger.Debug().Msgf("downloading files from dirs matching %s", hint)
	domains := keeper.ReadAvailableMatchingDomains(hint)
	keeper.logger.Info().Msgf("found %d matchig domains", len(domains))
	keep = tools.ToLower(keep)
	skip = tools.ToLower(skip)

	for i, domain := range domains {
		keeper.logger.Info().Msgf("retrieving domain %d %s", i, domain.url)
		keeper.downloadDomainFiles(ctx, domain, keep, skip)
	}
}

func (keeper *Keeper) Listen(ctx context.Context) {
	logger := tools.LoggerForFunction(keeper.logger, "Listen")
	logger.Info().Msg("keeper is listening")

	defer keeper.wg.Wait()
	checked := 0
	written := 0
	requested := 0
	clients := 0
	for {
		logger.Debug().Msg("select start")
		select {
		case domain := <-keeper.domainWriter:
			logger.Debug().Msg("writing domain record " + domain.url)
			if !keeper.writeDomain(domain) {
				logger.Error().Msg("failed to write domain to database: " + domain.url)
			}
			written++
			requested--
		case msg := <-keeper.fileWriter:
			logger.Debug().Msg("writing file record " + msg.Url())
			if !keeper.writeFile(msg.File(), msg.Action()) {
				logger.Error().Msg("failed to write file to database: " + msg.Url())
				continue
			}
			if msg.Body() != nil {
				keeper.saveFile(ctx, msg.Domain(), msg.File(), msg.Body())
			}
			written++
		case msg := <-keeper.recordChecker:
			logger.Debug().Msg("checking record")
			if msg.Action() == Init {
				clients++
				logger.Warn().Msgf("new client %d", clients)
				continue
			}

			if msg.Action() == End {
				clients--
				logger.Warn().Msgf("client deregistered %d", clients)
			} else {
				logger.Debug().Msg("checking url " + msg.Url())
				checked += 1
				if msg.Type() == RecordType(File) {
					keeper.updateFile(msg.File())
				}
			}
		case <-keeper.waiter:
			logger.Info().Msg("dummy received")
			logger.Info().Msgf("written/checked/requested/clients %d/%d/%d/%d", written, checked, requested, clients)

			if clients == 0 && keeper.fileFetcher == nil && keeper.domFetcher == nil {
				logger.Warn().Msg("exiting listener as per requests fulfilled")
				return
			}
		case <-ctx.Done():
			logger.Warn().Msg("exiting listener as per context finalisation")
			return
		}
		logger.Debug().Msgf("written/checked/requested/clients %d/%d/%d/%d", written, checked, requested, clients)

	}

}

func (keeper *Keeper) Close() {
	keeper.logger.Info().Msg("closing keeper")
	keeper.logger.Info().Msg("sending dummy")
	keeper.waiter <- true
	keeper.logger.Info().Msg("closing database and channels")
	close(keeper.domainWriter)
	close(keeper.recordChecker)
	close(keeper.fileWriter)
	close(keeper.fileChecker)
	close(keeper.waiter)
	keeper.db.Close()
}
