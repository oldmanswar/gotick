package bucket

import (
	"context"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

type oceanExplorer struct {
	logger *zerolog.Logger
	keeper *Keeper
}

func newOceanExplorer(ctx context.Context, keeper *Keeper) Explorer {
	explorer := new(oceanExplorer)
	explorer.logger = tools.LoggerForContext(ctx, "oceanExplorer")
	explorer.keeper = keeper

	return explorer
}

func (bkt *oceanExplorer) Word2Url(word string) string {
	return "https://" + word + ".digitaloceanspaces.com/"
}

func (bkt *oceanExplorer) WordAffix2Urls(word string, affix string) []string {
	urls := make([]string, 0, 4)

	urls = append(urls, "https://"+word+affix+".digitaloceanspaces.com/")
	urls = append(urls, "https://"+affix+word+".digitaloceanspaces.com/")
	urls = append(urls, "https://"+word+"-"+affix+".digitaloceanspaces.com/")
	urls = append(urls, "https://"+affix+"-"+word+".digitaloceanspaces.com/")

	return urls
}

func (xpl *oceanExplorer) Explore(ctx context.Context, words []string, affixes []string) {
	explore(ctx, xpl, words, affixes)
}

func (xpl *oceanExplorer) Logger() *zerolog.Logger {
	return xpl.logger
}

func (xpl *oceanExplorer) Keeper() *Keeper {
	return xpl.keeper
}
