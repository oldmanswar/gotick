package tools

import (
	"context"
	"net"
	"sync"
	"time"
)

type DNStealth struct {
	current   int64
	m         sync.Mutex
	resolvers []*net.Resolver
}

var stdDnsServers = [...]string{
	"8.8.8.8",        // google
	"208.67.222.222", // opendns
	"9.9.9.9",        // quad9
	"1.1.1.1",        // cloudflare
	"45.90.28.0",     // nextdns
	//"8.26.56.26",   // comodo secure
	//"192.95.54.3",	// opennic
	"185.228.168.168", // cleanbrowsing
}

func createResolver(ip string, duration int64) *net.Resolver {
	r := &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			d := net.Dialer{
				Timeout: time.Millisecond * time.Duration(duration),
			}
			return d.DialContext(ctx, network, ip+":53")
		},
	}

	return r
}

func CreateDNStealth(ips []string, duration int64) *DNStealth {
	var dnstealth DNStealth
	dnstealth.resolvers = make([]*net.Resolver, len(ips))
	for i, ip := range ips {
		dnstealth.resolvers[i] = createResolver(ip, duration)
	}
	return &dnstealth
}

func CreateStdDNStealth() *DNStealth {
	return CreateDNStealth(stdDnsServers[:], 10000)
}

func (dnstealth *DNStealth) NumberOfServers() int {
	return len(dnstealth.resolvers)
}

func (dnstealth *DNStealth) LookupHost(ctx context.Context, host string) ([]string, error) {
	addrs, err := dnstealth.resolvers[dnstealth.current].LookupHost(ctx, host)
	dnstealth.m.Lock()
	dnstealth.current++
	if int(dnstealth.current) == len(dnstealth.resolvers) {
		dnstealth.current = 0
	}
	dnstealth.m.Unlock()
	return addrs, err
}
