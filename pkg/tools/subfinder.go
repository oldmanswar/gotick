package tools

import (
	"bytes"
	"context"
	"io"
	"slices"
	"sort"
	"strings"

	"github.com/projectdiscovery/subfinder/v2/pkg/passive"
	"github.com/projectdiscovery/subfinder/v2/pkg/runner"
	"github.com/projectdiscovery/subfinder/v2/pkg/subscraping"
)

type Subfinder struct {
	noKeyOnly bool
	ignore    []string
	sources   []subscraping.Source
}

var dodgy []string = []string{"commoncrawl", "riddler", "crtsh",
	"hackertarget", "waybackarchive", "alienvault"}

func NormaliseSubs(subs []string) []string {
	for i, sub := range subs {
		subs[i] = strings.ReplaceAll(sub, ".", "-")
	}
	return subs
}

func CreateSubfinder(noKeyOnly bool, ignore []string) *Subfinder {
	var subfinder Subfinder

	subfinder.ignore = ignore
	subfinder.noKeyOnly = noKeyOnly

	for _, source := range passive.AllSources {
		if source.NeedsKey() || slices.Contains(ignore, source.Name()) {
			continue
		}
		subfinder.sources = append(subfinder.sources, source)
	}

	return &subfinder
}

func CreateStdSubfinder() *Subfinder {
	return CreateSubfinder(true, dodgy)
}

func (subfinder *Subfinder) SourceList() []string {
	var sourceNames []string
	for _, source := range subfinder.sources {
		sourceNames = append(sourceNames, source.Name())
	}
	return sourceNames
}

func (subfinder *Subfinder) SourceString() string {
	sourceNames := subfinder.sources[0].Name()
	for i := 1; i < len(subfinder.sources); i++ {
		sourceNames = sourceNames + "," + subfinder.sources[i].Name()
	}
	return sourceNames
}

func (subfinder *Subfinder) Run(ctx context.Context, domain string) []string {
	subfinderOpts := &runner.Options{
		Threads:            5,
		Timeout:            30,
		MaxEnumerationTime: 1,
		Sources:            subfinder.SourceList(),
	}
	runner, err := runner.NewRunner(subfinderOpts)
	if err != nil {
		return nil
	}
	output := &bytes.Buffer{}
	if err = runner.EnumerateSingleDomainWithCtx(context.Background(),
		domain, []io.Writer{output}); err != nil {
		return nil
	}
	subs := strings.Split(output.String(), "\n")
	var stubs []string
	for _, sub := range subs {
		cutAt := strings.Index(sub, domain) - 1
		if cutAt > 0 {
			stubs = append(stubs, sub[:cutAt])
		}
	}
	sort.Strings(stubs)
	return stubs
}
