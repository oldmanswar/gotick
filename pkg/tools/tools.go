package tools

import (
	"context"
	"hash/crc64"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog"
)

var affixes = []string{
	"dev", "development",
	"prod", "production",
	"qa", "stage", "staging",
	"data", "datastore", "datastorage",
	"store", "storage",
	"drive",
	"backup",
	"uploads", "upload",
	"public", "private",
}

var crc64Table *crc64.Table = crc64.MakeTable(crc64.ISO)

func FindHash(str string) int64 {
	var mtx sync.Mutex
	mtx.Lock()
	hash := crc64.Checksum([]byte(str), crc64Table)
	mtx.Unlock()
	return int64(hash)
}

func SetAffixes(newAffixes []string) {
	affixes = newAffixes
}

func Affixes() []string {
	return affixes
}

func ExpandWord(word string) []string {
	var words []string

	words = append(words, word)

	for _, affix := range affixes {
		words = append(words, word+"-"+affix)
		words = append(words, affix+"-"+word)
		words = append(words, word+affix)
		words = append(words, affix+word)
	}

	return words
}

func ExpandWords(words []string) []string {
	var newWords []string

	for _, word := range words {
		if word != "" {
			newWords = append(newWords, ExpandWord(word)...)
		}
	}

	return newWords
}

func DeepDuplicate(words []string) []string {
	newWords := make([]string, 0, len(words))
	for _, word := range words {
		if word != "" {
			newWords = append(newWords, word)
		}
	}
	return newWords
}

func LoggerForContext(ctx context.Context, entity string) *zerolog.Logger {
	logger := ctx.Value("logger").(*zerolog.Logger).With().Str("entity", entity).Logger()
	logger.Debug().Msg("Entering function.")
	return &logger
}

func LoggerForFunction(logger *zerolog.Logger, function string) *zerolog.Logger {
	newLogger := logger.With().Str("function", function).Logger()
	logger.Debug().Msg("Entering function.")
	return &newLogger
}

func UseProxy(proxy string) bool {
	proxyUrl, err := url.Parse(proxy)
	if err != nil {
		return false
	}
	http.DefaultTransport = &http.Transport{Proxy: http.ProxyURL(proxyUrl)}

	return true
}

func ToLower(strs []string) []string {
	for i, str := range strs {
		strs[i] = strings.ToLower(str)
	}
	return strs
}

func GetUrl(ctx context.Context, urlString string, dry_opt ...bool) ([]byte, int, string) {
	logger := LoggerForContext(ctx, "GetUrl")
	dry := true
	if len(dry_opt) > 0 {
		dry = dry_opt[0]
	}
	client := &http.Client{Timeout: 25 * time.Second}

	request, err := http.NewRequest("GET", urlString, nil)
	if err != nil {
		logger.Debug().Msg("Failed (request) http.get for " + urlString + " " + err.Error())
		return nil, 0, err.Error()
	}
	var body []byte
	resp, err := client.Do(request)
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		if resp != nil && resp.Body != nil {
			body, err = io.ReadAll(resp.Body)
			if err != nil {
				logger.Warn().Msg("Failed reading error body for " + urlString)
				return nil, -10, err.Error()
			}
		} else {
			logger.Debug().Msgf("tools do error: %s", err.Error())
		}
		errString := err.Error()
		// provide some "trimming" of the errors to help group them in the DB
		if strings.Contains(err.Error(), "no such host") {
			return body, 1, "no such host"
		}
		if strings.Contains(err.Error(), "context deadline exceeded") {
			return body, 2, "context deadline exceeded"
		}
		if strings.Contains(err.Error(), "connection refused") {
			logger.Warn().Msgf("connection refused for %s", urlString)
			return body, 3, "connection refused"
		}
		logger.Debug().Msg("Failed (Do) http.get for " + urlString + " " + errString)
		return nil, 4, errString
	}
	logger.Debug().Msgf("Proto: %s Status: %d %s", resp.Request.Proto, resp.StatusCode, urlString)
	if !dry {
		logger.Debug().Msg("retrieving file body")
		body, err = io.ReadAll(resp.Body)
		if err != nil {
			logger.Warn().Msg("Failed reading body for " + urlString)
			logger.Warn().Msg(err.Error())
			return nil, -10, err.Error()
		}
		return body, resp.StatusCode, ""
	}
	return nil, resp.StatusCode, ""
}
