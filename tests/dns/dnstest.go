package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

func main() {

	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		With().Timestamp().
		Str("application", "gotick.go").
		Logger()

	ctx := context.WithValue(context.Background(), "logger", &logger)
	dns := tools.CreateStdDNStealth()
	for i := 0; i < dns.NumberOfServers(); i++ {
		ip, err := dns.LookupHost(ctx, "ipinfo.io")
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println(ip)
		}
	}
}
