package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

func main() {

	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		With().Timestamp().
		Str("application", "gotick.go").
		Logger()

	ctx := context.WithValue(context.Background(), "logger", &logger)
	fmt.Println(tools.GetUrl(ctx, "https://ipinfo.io/ip", false))
	tools.UseProxy("socks5://localhost:9150/")
	fmt.Println(tools.GetUrl(ctx, "https://ipinfo.io/ip", false))
}
