package main

import (
	"context"
	"os"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

func main() {

	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		With().Timestamp().
		Str("application", "subdomaintest.go").
		Logger()

	ctx := context.WithValue(context.Background(), "logger", &logger)
	subdomain := tools.CreateStdSubfinder()
	sources := subdomain.SourceList()
	for _, source := range sources {
		logger.Debug().Msg(source)
	}

	results := subdomain.Run(ctx, "hackerone.com")
	for _, result := range results {
		logger.Warn().Msg(result)
	}
}
